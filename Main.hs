{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedStrings #-}

module Main where

import Options.Generic
import Data.Maybe
import Data.Time.Calendar
import Data.Time.Calendar.WeekDate (toWeekDate)
import Data.Time.Clock (getCurrentTime, utctDay)
import Text.Megaparsec
import qualified Text.Megaparsec.Char.Lexer as L 
import Data.Text
import Data.Void
import Text.Megaparsec.Char
import Control.Monad (void)


type Parser = Parsec Void Text

data Input = Input (Maybe Text)
    deriving (Generic, Show)

instance ParseRecord Input

weekOfYear :: Day -> Int
weekOfYear = (\(_,b,_) -> b ) . toWeekDate 

parseDay :: Parser Day
parseDay = do
    year <- L.decimal 
    void (char '-')
    month <- L.decimal 
    void (char '-')
    day <- L.decimal 
    return $ fromGregorian year month day

printCurrentWeekofYear :: IO () 
printCurrentWeekofYear = do 
      current <- fmap utctDay getCurrentTime
      putStrLn $ show $ weekOfYear current
      
printWeekofYear datestring = do 
      let d = parseMaybe parseDay datestring
      case d of
          Nothing -> putStrLn "Error, unable to parse date YYYY-MM-DD"
          Just da -> putStrLn $ show $ weekOfYear da

main = do
    date <- getRecord "kw [YYYY-MM-DD] \n Returns week of year of today or of given date" 
    case date of
        Nothing -> printCurrentWeekofYear
        Just datestring -> printWeekofYear datestring
             

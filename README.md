# calenderweek

Simple commandline tool to print week of year.
If nothing is provided, the current week is printed.
To query the week of year for a specific date, provide the date in ISO 8601 (YYYY-MM-DD).

## Why

Week of year is often used for communicating a time span. "We are planning to finish the feature in week 50" is a short and precise way to say that we finish between the 9th and the 15th december of 2019. Many Calender programs, like Outlook, are not providing this number. 

## Example 


    kw 2019-11-25
    >48

    kw                           #week of today
    >48

    kw -h

    >kw [YYYY-MM-DD] Returns week of year of today or of given date
    
    >Usage: kw [TEXT]
    
    >Available options:
    -h,--help                Show this help text
 
